package com.mahasiswa;

public class MahasiswaAksi {
    public static void main(String[] args) {
        Mahasiswa mahasiswa = new Mahasiswa("A11.2020.13118", "Ezra Farid", 3.65, 88, "2002-03-18");
        System.out.println("Nama Program Studi : " + mahasiswa.getProgdi());
        System.out.println("Status : " + mahasiswa.ipkStatus());
        System.out.println("Tahun Angkatan : " + mahasiswa.getTahun());
        System.out.println("Tagihan : " + mahasiswa.getTagihanSks());
        System.out.println("Semester : " + mahasiswa.getMhsSemester());
        System.out.println("Umur : " + mahasiswa.getUmur());

    }
}
